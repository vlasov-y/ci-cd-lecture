CI/CD
===

- [CI/CD](#cicd)
  - [General overview](#general-overview)
  - [Environment flow](#environment-flow)
  - [Software lifecycle flow](#software-lifecycle-flow)
  - [Modern tools](#modern-tools)
  - [Jenkins](#jenkins)
  - [Scripting](#scripting)
  - [Why Docker is so often used?](#why-docker-is-so-often-used)
  - [What is orchestartion?](#what-is-orchestartion)
  - [Example](#example)

## General overview

There is a good article [RU](https://tinyurl.com/yywwza7c)/[EN](https://blog.gds-gov.tech/that-ci-cd-thing-principles-implementation-tools-aa8e77f9a350) about *what is CI/CD*.  
Often, people associate CI/CD with [Docker](https://techrocks.ru/2019/04/19/docker-guide-for-beginners/) containerization. It is due to the great popularity of Docker Engine.
But **CI/CD does not require Containerization**.

## Environment flow

```mermaid
graph LR
  dev(Development) --> qa(Quality<br>Assurence) --> po(Product<br>Owner) --> user(End<br>User)
  qa -. Bug found .-> dev
  po -. Feature request .-> dev
```

## Software lifecycle flow

```mermaid
graph LR
  trigger(Trigger) --> static(Static<br>analysis) --> build(Build) --> test(UnitTest) --> deploy(Deploy) --> uninstall(Update) 
```

Lifecycle of application end not when it is deployed, but when it was successfully replaced with newer version.

## Modern tools

- GitLabCI
- TeamCity
- Jenkins
- GitHub Actions
- BitBucket CI
- etc.
  
There are many tools that could be used for making automated software delivery pipelines. Each one has its cons and pros.

## Jenkins
Old, well-known solution. Supports [IaC](https://en.wikipedia.org/wiki/Infrastructure_as_code) and many community plugins.

## Scripting

**Bash** - is probably to most common solution which is used for scripting. Also Powershell, Python, Lua. Any language you want will be good choice.

## Why Docker is so often used?

There are many valuable **pros** which you obtain using containers:

1. Isolation - containers are isolated from each other and host OS too
2. Dependecies - every applications is provided with all needed dependencies
3. Simple installation - pulling of image *OverlayFS* and creating of container is quite simple operation
4. Easy blue/green deployment
5. Easy version control
6. Easy horizontal scaling  
  
But there are also some **cons** which you also have to worry about:

1. Network communication - overlay neworks, ports controlling. Services now communicate with each other like separated servers
2. Lower performance - it is only valuable for highload blockchain applications

## What is orchestartion?

- Kubernetes
- Docker Swarm
- AWS Fargate
- Rancher

## Example

[Click here.](https://gitlab.com/vlasov-y/convoting)